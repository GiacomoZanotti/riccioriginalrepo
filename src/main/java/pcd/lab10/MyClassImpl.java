package pcd.lab10;

public class MyClassImpl implements java.io.Serializable, MyClass  {

	private int x;
	
	public MyClassImpl(int x){
		this.x = x;
	}
	
	public int get(){
		return x;
	}
	
	public void update(int c){
		x = c;
		// while (true){}
	}
}
