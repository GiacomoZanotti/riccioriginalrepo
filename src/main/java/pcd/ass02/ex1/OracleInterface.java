package pcd.ass02.ex1;

public interface OracleInterface {

	boolean isGameFinished();
	
	Result tryToGuess(int playerId, long value) throws GameFinishedException;
	
}
