package pcd.ass02.ex1.sol;

import java.util.Random;

import pcd.ass02.ex1.GameFinishedException;
import pcd.ass02.ex1.OracleInterface;
import pcd.ass02.ex1.Result;

public class OracleImpl implements OracleInterface {

	private int nPlayers;
	private boolean[] hasToPlay;
	private int nGuesses;
	private long secretNumber;
	private boolean gameFinished;

	public OracleImpl(int nPlayers) {
		nGuesses = 0;
		this.nPlayers = nPlayers;
		hasToPlay = new boolean[nPlayers];
		resetPlay();
		Random gen = new Random();
		secretNumber = Math.abs(gen.nextLong());
		log("the secret is: " + secretNumber);
		gameFinished = false;
	}

	@Override
	public synchronized boolean isGameFinished() {
		return gameFinished;
	}

	@Override
	public synchronized Result tryToGuess(int playerId, long value) throws GameFinishedException {
		if (!gameFinished) {
			while (!hasToPlay[playerId] && !gameFinished) {
				try {
					wait();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			if (!gameFinished) {
				log("Player " + playerId + " guessed " + value);
				hasToPlay[playerId] = false;
				nGuesses++;
				Result res = new ResultImpl(secretNumber, value);
				if (res.found()) {
					gameFinished = true;
				} else if (nGuesses == nPlayers) {
					nGuesses = 0;
					resetPlay();
				}
				notifyAll();
				return res;
			} else {
				throw new GameFinishedException();
			}
		} else {
			throw new GameFinishedException();
		}
	}

	private void resetPlay() {
		for (int i = 0; i < hasToPlay.length; i++) {
			hasToPlay[i] = true;
		}
	}

	protected void log(String msg) {
		synchronized (System.out) {
			System.out.println("[ ORACLE ] " + msg);
		}
	}

}
