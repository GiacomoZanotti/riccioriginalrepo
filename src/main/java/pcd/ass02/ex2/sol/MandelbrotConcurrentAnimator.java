package pcd.ass02.ex2.sol;

import pcd.ass02.ex2.Complex;

/**
 * Concurrent Mandelbrot Set Viewer 
 *		 
 * @author aricci
 *
 */
public class MandelbrotConcurrentAnimator {
	public static void main(String[] args) throws Exception {
		
		/* size of the mandelbrot set in pixel */
		int width = 800;
		int height = 600;	
		
		double rad0 = 2;

		/* region to be represented: center and radius */
		Complex c0 = new Complex(-0.75,0);
		Complex c1 = new Complex(-0.75,0.1);
		Complex c2 = new Complex(-0.1011,0.9563);
		Complex c3 = new Complex(0.254,0);
		Complex c4 = new Complex(0.001643721971153, 0.822467633298876);
		
		/* creating the set */
		MandelbrotSetImage set = new MandelbrotSetImageImplOpt(width,height, c4, rad0);
		MandelbrotView view = new MandelbrotView(set,810,680);
		Controller controller = new Controller(set,view);
		view.setController(controller);
		view.setVisible(true);

	}

}
