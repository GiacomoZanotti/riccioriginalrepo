package pcd.lab04.gui2_mvc;

public interface ModelObserver {

	void modelUpdated(MyModel model);
}
