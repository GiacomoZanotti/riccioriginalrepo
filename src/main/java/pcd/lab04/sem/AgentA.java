package pcd.lab04.sem;

import java.util.concurrent.*;

public class AgentA extends Thread {
	private Semaphore ev;
	
	public AgentA(Semaphore ev){
		this.ev = ev;
	}
	
	public void run(){
		for (int i = 0; i < 5; i++){
			System.out.println("Hello");
			try {
				Thread.sleep(100);
			} catch (Exception ex){}
			ev.release();
		}
	}

}
