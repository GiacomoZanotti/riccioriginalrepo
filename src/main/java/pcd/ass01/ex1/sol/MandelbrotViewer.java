package pcd.ass01.ex1.sol;

import java.util.LinkedList;
import java.util.List;

/**
 * Simple Mandelbrot Set Viewer 
 *		 
 * @author aricci
 *
 */
public class MandelbrotViewer {
	public static void main(String[] args) throws Exception {
		
		/* size of the mandelbrot set in pixel */
		int width = 8000;
		int height = 8000;	
		
		/* number of iteration */
		int nIter = 500;

		/* region to be represented: center and radius */
		Complex c0 = new Complex(-0.75,0);
		double rad0 = 2;

		/*
		Complex c1 = new Complex(-0.75,0.1);
		double rad1 = 0.02;
		
		Complex c2 = new Complex(0.7485,0.0505);
		double rad2 = 0.000002;

		Complex c3 = new Complex(0.254,0);
		double rad3 = 0.001;
	
		*/
		
		/* creating the set */
		MandelbrotSetImage set = new MandelbrotSetImageImplOpt(width,height, c0, rad0);

		System.out.println("Computing w:"+width+"|h:"+height+"|nIt:"+nIter+"...");
		StopWatch cron = new StopWatch();
		cron.start();
		
		/* computing the image */

		int nWorkers = Runtime.getRuntime().availableProcessors() + 1;
		List<MandelbrotWorker> workers = new LinkedList<MandelbrotWorker>();
		for (int i = 0; i < nWorkers; i++){
			MandelbrotWorker worker = new MandelbrotWorker(set, nIter, i, nWorkers);
			workers.add(worker);
			worker.start();
		}
		
		for (MandelbrotWorker worker: workers){
			worker.join();
		}		
		
		cron.stop();
		System.out.println("done - "+cron.getTime()+" ms");

		/* showing the image */
		MandelbrotView view = new MandelbrotView(set,800,600);
		view.setVisible(true);
		

	}

}
